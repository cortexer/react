#!/bin/env node
//  OpenShift sample Node application
var restify = require('restify');
var mongojs = require("mongojs");

var ip_addr = process.env.OPENSHIFT_NODEJS_IP   || '127.0.0.1';
var port    = process.env.OPENSHIFT_NODEJS_PORT || '8080';

var db_name = process.env.OPENSHIFT_APP_NAME || "localposts";

var connection_string = '127.0.0.1:27017/' + db_name;
// if OPENSHIFT env variables are present, use the available connection info:
if(process.env.OPENSHIFT_MONGODB_DB_PASSWORD){
  connection_string = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
  process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
  process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
  process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
  process.env.OPENSHIFT_APP_NAME;
}

var db = mongojs(connection_string, [db_name]);
var posts = db.collection("posts");


var server = restify.createServer({
    name : "localposts"
});

server.pre(restify.pre.userAgentConnection());
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());

function getPage(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.send('success');
}

function findAllposts(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    posts.find().limit(20).sort({postedOn : -1} , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(200 , success);
            return next();
        }else{
            return next(err);
        }
        
    });
    
}

function findPost(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    posts.findOne({_id:mongojs.ObjectId(req.params.postId)} , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(200 , success);
            return next();
        }
        return next(err);
    })
}

function updatePost(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    posts.findOne({_id:mongojs.ObjectId(req.params.postId)} , function(err , post){
    if (err)
      res.send(err);

    // Update the existing name
	//post.postId = req.params.postId;
	if (req.params.nimi) {
	 	post.nimi = req.params.nimi;
	}
	if (req.params.imgurl) {
	 	post.imgurl = req.params.imgurl;
	}
	if (req.params.replies) {
	 	post.replies = req.params.replies;
	}
	if (req.params.points) {
	 	post.points = req.params.points;
	}
		
    post.editedAt = req.params.editedAt;	

    // Save the post and check for errors
    posts.save(post , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(201 , req.params);
            return next();
        }else{
            return next(err);
        }
    });
  });
};

function postNewPost(req , res , next){
    var post = {};
    post.nimi = req.params.nimi;
	post.imgurl = req.params.imgurl;
    post.parent = req.params.parent;
	post.points = req.params.points;
	post.editedAt = req.params.editedAt;
	post.password = req.params.password;
    post.replies = req.params.replies;

    res.setHeader('Access-Control-Allow-Origin','*');
    
    posts.save(post , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(201 , post);
            return next();
        }else{
            return next(err);
        }
    });
}

function deletePost(req , res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    posts.remove({_id:mongojs.ObjectId(req.params.postId)} , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(204);
            return next();      
        } else{
            return next(err);
        }
    })
    
}



var PATH = '/posts'

server.get({path : PATH , version : '0.0.1'} , findAllposts);
server.get({path : PATH +'/:postId' , version : '0.0.1'} , findPost);
server.post({path : PATH , version: '0.0.1'} ,postNewPost);
server.del({path : PATH +'/:postId' , version: '0.0.1'} ,deletePost);
server.put({path : PATH +'/:postId' , version: '0.0.1'} ,updatePost);

var PATH = '/test'
server.get({path : PATH , version : '0.0.1'} , getPage);

var defaultDoc = "index.html";

server.get('/', restify.serveStatic({
  directory: './',
  default: 'index.html'
}));

server.listen(port ,ip_addr, function(){
    console.log('%s listening at %s ', server.name , server.url);
})

